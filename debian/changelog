rocsolver (5.5.1-7) unstable; urgency=medium

  * Upload to unstable.

 -- Cordell Bloor <cgmb@slerp.xyz>  Wed, 19 Feb 2025 23:21:13 -0700

rocsolver (5.5.1-7~exp1) experimental; urgency=medium

  * Add librocsolver0-bench package
  * Add d/p/0011-fmt-10-support.patch to fix FTBFS with
    libfmt 10 or later.
  * d/rules: only skip install rpath
  * d/rules: only run fast tests at build time

 -- Cordell Bloor <cgmb@slerp.xyz>  Sun, 24 Nov 2024 15:22:36 -0700

rocsolver (5.5.1-6) unstable; urgency=medium

  * Add d/p/0010-drop-f16c-instructions.patch to prevent the unguarded use of
    AVX instructions on systems that don't support them
  * Update Standards-Version to 4.7.0. No changes required
  * Update upstream URLs
  * Fix symbols-file-contains-current-version-with-debian-revision lintian
    error by marking C++ standard library function as optional

 -- Cordell Bloor <cgmb@slerp.xyz>  Mon, 30 Sep 2024 06:25:54 -0600

rocsolver (5.5.1-5) unstable; urgency=medium

  [ Cordell Bloor ]
  * Migrate to unstable
  * Add d/gbp.conf to ensure use of pristine-tar
  * Add d/p/0006-rm-immintrin-include.patch to fix ppc64el build
  * Add d/p/0007-fix-reserved-identifiers.patch to backport upstream
    fix for reserved identifiers in rocblas headers
  * Add d/p/0008-check-for-hip-errors.patch to ensure HIP runtime return
    values are checked for errors
  * Add d/p/0009-verbose-build-of-specialized-kernels.patch to prevent
    buildd timeouts on ppc64el

  [ Christian Kastner ]
  * d/control: Add missing Breaks+Replaces for moved examples
  * autopkgtest: Export dmesg and other info as artifacts

 -- Cordell Bloor <cgmb@slerp.xyz>  Tue, 19 Mar 2024 11:51:09 -0600

rocsolver (5.5.1-5~exp1) experimental; urgency=medium

  * Enable gfx1100, gfx1101, and gfx1102 architectures
  * Enable specialized kernels for improved performance on
    small matrices
  * Update Build-Depends for clang-17
  * Drop patchelf from Build-Depends
  * Use compressed DWARF5 debug symbols
  * Link rocsolver-test against generic liblapack.so.3 rather than
    specifically linking against OpenBLAS
  * Move examples from dev package to doc package

 -- Cordell Bloor <cgmb@slerp.xyz>  Sat, 02 Mar 2024 08:53:11 -0700

rocsolver (5.5.1-4) unstable; urgency=medium

  * Re-enable tests on gfx1034
    The soft lockup was apparently due to a faulty device.

 -- Christian Kastner <ckk@debian.org>  Fri, 10 Nov 2023 10:09:17 +0100

rocsolver (5.5.1-3) unstable; urgency=medium

  * Auto-fail tests on gfx1034 until soft lockup is resolved

 -- Christian Kastner <ckk@debian.org>  Sat, 23 Sep 2023 10:47:41 +0200

rocsolver (5.5.1-2) unstable; urgency=medium

  * Upload to unstable.

  * Filter cf-protection hardening from device code.
    Fixes a FTBFS with dpkg >= 1.22
  * Add 0005-doxygen-Add-parent-directory-to-inputs.patch
    Fixes a FTBFS with newer doxygen.
  * Add myself to Uploaders

 -- Christian Kastner <ckk@debian.org>  Thu, 14 Sep 2023 08:54:58 +0200

rocsolver (5.5.1-1) experimental; urgency=medium

  * Initial release. (Closes: #1023081)

 -- Cordell Bloor <cgmb@slerp.xyz>  Tue, 18 Jul 2023 02:02:35 -0600
