Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: rocSOLVER
Source: https://github.com/ROCm/rocSOLVER

Files: *
Copyright: 2016-2023, Advanced Micro Devices, Inc.
License: BSD-2-clause

Files: debian/*
Copyright: 2022, Maxime Chambonnet <maxzor@maxzor.eu>
           2022-2024, Cordell Bloor <cgmb@slerp.xyz>
License: Expat

Files: library/src/auxiliary/rocauxiliary_bdsqr.hpp
       library/src/auxiliary/rocauxiliary_bdsvdx.hpp
       library/src/auxiliary/rocauxiliary_labrd.hpp
       library/src/auxiliary/rocauxiliary_lacgv.hpp
       library/src/auxiliary/rocauxiliary_larf.hpp
       library/src/auxiliary/rocauxiliary_larfb.hpp
       library/src/auxiliary/rocauxiliary_larfg.hpp
       library/src/auxiliary/rocauxiliary_larft.hpp
       library/src/auxiliary/rocauxiliary_laswp.hpp
       library/src/auxiliary/rocauxiliary_lasyf.hpp
       library/src/auxiliary/rocauxiliary_latrd.hpp
       library/src/auxiliary/rocauxiliary_org2l_ung2l.hpp
       library/src/auxiliary/rocauxiliary_org2r_ung2r.hpp
       library/src/auxiliary/rocauxiliary_orgbr_ungbr.hpp
       library/src/auxiliary/rocauxiliary_orgl2_ungl2.hpp
       library/src/auxiliary/rocauxiliary_orglq_unglq.hpp
       library/src/auxiliary/rocauxiliary_orgql_ungql.hpp
       library/src/auxiliary/rocauxiliary_orgqr_ungqr.hpp
       library/src/auxiliary/rocauxiliary_orgtr_ungtr.hpp
       library/src/auxiliary/rocauxiliary_orm2l_unm2l.hpp
       library/src/auxiliary/rocauxiliary_orm2r_unm2r.hpp
       library/src/auxiliary/rocauxiliary_ormbr_unmbr.hpp
       library/src/auxiliary/rocauxiliary_orml2_unml2.hpp
       library/src/auxiliary/rocauxiliary_ormlq_unmlq.hpp
       library/src/auxiliary/rocauxiliary_ormql_unmql.hpp
       library/src/auxiliary/rocauxiliary_ormqr_unmqr.hpp
       library/src/auxiliary/rocauxiliary_ormtr_unmtr.hpp
       library/src/auxiliary/rocauxiliary_stebz.hpp
       library/src/auxiliary/rocauxiliary_stedc.hpp
       library/src/auxiliary/rocauxiliary_stein.hpp
       library/src/auxiliary/rocauxiliary_steqr.hpp
       library/src/auxiliary/rocauxiliary_sterf.hpp
       library/src/lapack/roclapack_gebd2.hpp
       library/src/lapack/roclapack_gebrd.hpp
       library/src/lapack/roclapack_gelq2.hpp
       library/src/lapack/roclapack_gelqf.hpp
       library/src/lapack/roclapack_gels.hpp
       library/src/lapack/roclapack_gels_outofplace.hpp
       library/src/lapack/roclapack_geql2.hpp
       library/src/lapack/roclapack_geqlf.hpp
       library/src/lapack/roclapack_geqr2.hpp
       library/src/lapack/roclapack_geqrf.hpp
       library/src/lapack/roclapack_gerq2.hpp
       library/src/lapack/roclapack_gerqf.hpp
       library/src/lapack/roclapack_gesv.hpp
       library/src/lapack/roclapack_gesv_outofplace.hpp
       library/src/lapack/roclapack_gesvd.hpp
       library/src/lapack/roclapack_gesvdj.hpp
       library/src/lapack/roclapack_gesvdj_notransv.hpp
       library/src/lapack/roclapack_gesvdx.hpp
       library/src/lapack/roclapack_getf2.hpp
       library/src/lapack/roclapack_getrf.hpp
       library/src/lapack/roclapack_getri.hpp
       library/src/lapack/roclapack_getrs.hpp
       library/src/lapack/roclapack_posv.hpp
       library/src/lapack/roclapack_potf2.hpp
       library/src/lapack/roclapack_potrf.hpp
       library/src/lapack/roclapack_potri.hpp
       library/src/lapack/roclapack_potrs.hpp
       library/src/lapack/roclapack_syev_heev.hpp
       library/src/lapack/roclapack_syevd_heevd.hpp
       library/src/lapack/roclapack_syevdx_heevdx_inplace.hpp
       library/src/lapack/roclapack_syevx_heevx.hpp
       library/src/lapack/roclapack_sygs2_hegs2.hpp
       library/src/lapack/roclapack_sygst_hegst.hpp
       library/src/lapack/roclapack_sygv_hegv.hpp
       library/src/lapack/roclapack_sygvd_hegvd.hpp
       library/src/lapack/roclapack_sygvdx_hegvdx_inplace.hpp
       library/src/lapack/roclapack_sygvj_hegvj.hpp
       library/src/lapack/roclapack_sygvx_hegvx.hpp
       library/src/lapack/roclapack_sytd2_hetd2.hpp
       library/src/lapack/roclapack_sytf2.hpp
       library/src/lapack/roclapack_sytrd_hetrd.hpp
       library/src/lapack/roclapack_sytrf.hpp
       library/src/lapack/roclapack_trtri.hpp
       library/src/specialized/roclapack_getri_specialized_kernels.hpp
       library/src/specialized/roclapack_trtri_specialized_kernels.hpp
Copyright: 2019-2023, Advanced Micro Devices, Inc.
           2012,2013,2016,2017,2019, Univ. of Tennessee,
           2012,2013,2016,2017,2019, Univ. of California Berkeley,
           2012,2013,2016,2017,2019, Univ. of Colorado Denver
           2012,2013,2016,2017,2019, NAG Ltd.
License: BSD-2-clause and BSD-3-clause-lapack

Files: library/src/lapack/roclapack_syevj_heevj.hpp
Copyright: 2021-2022, Advanced Micro Devices, Inc.
License: BSD-2-clause
Comment:
 This file is noted as being derived from Gotlub & Van Loan (1996).
 Matrix Computations (3rd ed.) John Hopkins University Press. Section 8.4.
 and Hari & Kovac (2019). On the Convergence of Complex Jacobi Methods.
 Linear and Multilinear Algebra 69(3), p. 489-514. The referenced works
 describe algorithms.

Files: library/src/specialized/roclapack_getf2_specialized_kernels.hpp
Copyright: 2021-2022, Advanced Micro Devices, Inc.
           2009-2021, The University of Tennessee
License: BSD-2-clause and BSD-3-clause
Comment:
 This file is noted as being based on the algorithm from
 Abdelfattah, A., Haidar, A., Tomov, S., & Dongarra, J. (2017).
 Factorization and inversion of a million matrices using GPUs: Challenges
 and countermeasures. Procedia Computer Science, 108, 606-615. However,
 older versions of the file mentioned it was derived from MAGMA and
 the rocSOLVER licence file still notes that it includes code derived
 from MAGMA under the BSD-3-clause license.

Files: clients/rocblascommon/program_options.hpp
Copyright: 2020-2021, Advanced Micro Devices, Inc.
           2002-2004, Vladimir Prus
License: BSD-2-clause
Comment:
 This file mentions that it emulates the required functionality of
 boost::program_options (which is copyright 2002-2004 Vladimir Prus
 and licensed BSL-1.0).

License: BSD-2-clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions
 are met:
 1. Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.
 2. Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in the
    documentation and/or other materials provided with the distribution.
 .
 THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 SUCH DAMAGE.

License: BSD-3-clause-lapack
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are
 met:
 .
 - Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
 .
 - Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer listed
   in this license in the documentation and/or other materials
   provided with the distribution.
 .
 - Neither the name of the copyright holders nor the names of its
   contributors may be used to endorse or promote products derived from
   this software without specific prior written permission.
 .
 The copyright holders provide no reassurances that the source code
 provided does not infringe any patent, copyright, or any other
 intellectual property rights of third parties.  The copyright holders
 disclaim any liability to any recipient for claims brought against
 recipient by any third party for infringement of that parties
 intellectual property rights.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

License: BSD-3-clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are
 met:
 .
 - Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
 .
 - Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer listed
   in this license in the documentation and/or other materials
   provided with the distribution.
 .
 - Neither the name of the copyright holders nor the names of its
   contributors may be used to endorse or promote products derived from
   this software without specific prior written permission.
 .
 This software is provided by the copyright holders and contributors
 "as is" and any express or implied warranties, including, but not
 limited to, the implied warranties of merchantability and fitness for
 a particular purpose are disclaimed. in no event shall the copyright
 owner or contributors be liable for any direct, indirect, incidental,
 special, exemplary, or consequential damages (including, but not
 limited to, procurement of substitute goods or services; loss of use,
 data, or profits; or business interruption) however caused and on any
 theory of liability, whether in contract, strict liability, or tort
 (including negligence or otherwise) arising in any way out of the use
 of this software, even if advised of the possibility of such damage.

License: Expat
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.
