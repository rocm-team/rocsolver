From c2c43c9c7c231375d5f694a0bc821bf8bd11ccb9 Mon Sep 17 00:00:00 2001
From: Cory Bloor <Cordell.Bloor@amd.com>
Date: Thu, 26 Jan 2023 22:06:31 -0700
Subject: [PATCH] Check HIP API return values on all calls (#493)

* Check HIP API return values on all calls

* Check for HIP errors in SYEVJ/HEEVJ

* Drop padding checks from d_vector

* Fix error logging on success

Applied-Upstream: https://github.com/ROCm/rocSOLVER/commit/c2c43c9c7c231375d5f694a0bc821bf8bd11ccb9
---
 clients/gtest/memory_model_gtest.cpp          |  8 +-
 clients/include/testing_managed_malloc.hpp    | 50 ++++++------
 clients/rocblascommon/d_vector.hpp            | 77 +------------------
 common/CMakeLists.txt                         |  5 +-
 common/include/common_host_helpers.hpp        | 25 ++++--
 common/src/common_host_helpers.cpp            | 20 ++++-
 .../roclapack_syevdx_heevdx_inplace.hpp       | 11 ++-
 library/src/lapack/roclapack_syevj_heevj.hpp  | 12 ++-
 .../roclapack_sygvdx_hegvdx_inplace.hpp       | 11 ++-
 9 files changed, 93 insertions(+), 126 deletions(-)

--- a/clients/gtest/memory_model_gtest.cpp
+++ b/clients/gtest/memory_model_gtest.cpp
@@ -275,7 +275,7 @@
     // 4. pass user owned workspace (2MB)
     void* W;
     size_t sw = 2000000;
-    hipMalloc(&W, sw);
+    ASSERT_EQ(hipMalloc(&W, sw), hipSuccess);
     ASSERT_EQ(rocblas_set_workspace(handle, W, sw), rocblas_status_success);
 
     // 5. memory should now be user managed
@@ -315,9 +315,9 @@
     EXPECT_EQ(size, 2000000);
 
     // 14. pass larger user owned workspace
-    hipFree(W);
+    ASSERT_EQ(hipFree(W), hipSuccess);
     sw = 100000000;
-    hipMalloc(&W, sw);
+    ASSERT_EQ(hipMalloc(&W, sw), hipSuccess);
     ASSERT_EQ(rocblas_set_workspace(handle, W, sw), rocblas_status_success);
 
     // 15. 100MB should be reserved
@@ -333,6 +333,6 @@
     EXPECT_EQ(size, 100000000);
 
     // 18. destroy handle
-    hipFree(W);
+    ASSERT_EQ(hipFree(W), hipSuccess);
     EXPECT_EQ(rocblas_destroy_handle(handle), rocblas_status_success);
 }
--- a/clients/include/testing_managed_malloc.hpp
+++ b/clients/include/testing_managed_malloc.hpp
@@ -85,7 +85,7 @@
     // GPU lapack
     CHECK_ROCBLAS_ERROR(rocsolver_labrd(handle, m, n, nb, dARes, lda, dD, dE, dTauq, dTaup, dXRes,
                                         ldx, dYRes, ldy));
-    hipDeviceSynchronize();
+    CHECK_HIP_ERROR(hipDeviceSynchronize());
 
     // CPU lapack
     cpu_labrd(m, n, nb, dA, lda, dD, dE, dTauq, dTaup, dX, ldx, dY, ldy);
@@ -147,7 +147,7 @@
 
         CHECK_ROCBLAS_ERROR(rocsolver_labrd(handle, m, n, nb, dARes, lda, dD, dE, dTauq, dTaup,
                                             dXRes, ldx, dYRes, ldy));
-        hipDeviceSynchronize();
+        CHECK_HIP_ERROR(hipDeviceSynchronize());
     }
 
     // gpu-lapack performance
@@ -171,7 +171,7 @@
 
         start = get_time_us_sync(stream);
         rocsolver_labrd(handle, m, n, nb, dARes, lda, dD, dE, dTauq, dTaup, dXRes, ldx, dYRes, ldy);
-        hipDeviceSynchronize();
+        CHECK_HIP_ERROR(hipDeviceSynchronize());
         *gpu_time_used += get_time_us_sync(stream) - start;
     }
     *gpu_time_used /= hot_calls;
@@ -195,8 +195,8 @@
 
     // check managed memory enablement
     int deviceID, hmm_enabled;
-    hipGetDevice(&deviceID);
-    hipDeviceGetAttribute(&hmm_enabled, hipDeviceAttributeManagedMemory, deviceID);
+    CHECK_HIP_ERROR(hipGetDevice(&deviceID));
+    CHECK_HIP_ERROR(hipDeviceGetAttribute(&hmm_enabled, hipDeviceAttributeManagedMemory, deviceID));
     if(!hmm_enabled)
     {
         std::puts("Managed memory not enabled on device. Skipping test...");
@@ -254,16 +254,16 @@
     // memory allocations
     S *dD, *dE;
     T *dA, *dARes, *dTauq, *dTaup, *dX, *dXRes, *dY, *dYRes;
-    hipMallocManaged(&dA, sizeof(T) * size_A);
-    hipMallocManaged(&dARes, sizeof(T) * size_A);
-    hipMallocManaged(&dD, sizeof(S) * size_D);
-    hipMallocManaged(&dE, sizeof(S) * size_E);
-    hipMallocManaged(&dTauq, sizeof(T) * size_Q);
-    hipMallocManaged(&dTaup, sizeof(T) * size_P);
-    hipMallocManaged(&dX, sizeof(T) * size_X);
-    hipMallocManaged(&dXRes, sizeof(T) * size_X);
-    hipMallocManaged(&dY, sizeof(T) * size_Y);
-    hipMallocManaged(&dYRes, sizeof(T) * size_Y);
+    CHECK_HIP_ERROR(hipMallocManaged(&dA, sizeof(T) * size_A));
+    CHECK_HIP_ERROR(hipMallocManaged(&dARes, sizeof(T) * size_A));
+    CHECK_HIP_ERROR(hipMallocManaged(&dD, sizeof(S) * size_D));
+    CHECK_HIP_ERROR(hipMallocManaged(&dE, sizeof(S) * size_E));
+    CHECK_HIP_ERROR(hipMallocManaged(&dTauq, sizeof(T) * size_Q));
+    CHECK_HIP_ERROR(hipMallocManaged(&dTaup, sizeof(T) * size_P));
+    CHECK_HIP_ERROR(hipMallocManaged(&dX, sizeof(T) * size_X));
+    CHECK_HIP_ERROR(hipMallocManaged(&dXRes, sizeof(T) * size_X));
+    CHECK_HIP_ERROR(hipMallocManaged(&dY, sizeof(T) * size_Y));
+    CHECK_HIP_ERROR(hipMallocManaged(&dYRes, sizeof(T) * size_Y));
 
     // check quick return
     if(m == 0 || n == 0 || nb == 0)
@@ -289,16 +289,16 @@
                                       hot_calls, argus.profile, argus.profile_kernels, argus.perf);
 
     // free memory
-    hipFree(dA);
-    hipFree(dARes);
-    hipFree(dD);
-    hipFree(dE);
-    hipFree(dTauq);
-    hipFree(dTaup);
-    hipFree(dX);
-    hipFree(dXRes);
-    hipFree(dY);
-    hipFree(dYRes);
+    CHECK_HIP_ERROR(hipFree(dA));
+    CHECK_HIP_ERROR(hipFree(dARes));
+    CHECK_HIP_ERROR(hipFree(dD));
+    CHECK_HIP_ERROR(hipFree(dE));
+    CHECK_HIP_ERROR(hipFree(dTauq));
+    CHECK_HIP_ERROR(hipFree(dTaup));
+    CHECK_HIP_ERROR(hipFree(dX));
+    CHECK_HIP_ERROR(hipFree(dXRes));
+    CHECK_HIP_ERROR(hipFree(dY));
+    CHECK_HIP_ERROR(hipFree(dYRes));
 
     // validate results for rocsolver-test
     // using nb * max(m,n) * machine_precision as tolerance
--- a/clients/rocblascommon/d_vector.hpp
+++ b/clients/rocblascommon/d_vector.hpp
@@ -11,6 +11,7 @@
 #include <fmt/ostream.h>
 #include <rocblas/rocblas.h>
 
+#include "common_host_helpers.hpp"
 #include "rocblas_init.hpp"
 #include "rocblas_test.hpp"
 
@@ -29,25 +30,11 @@
         return size;
     }
 
-#ifdef ROCSOLVER_CLIENTS_TEST
-    U guard[PAD];
-    d_vector(size_t s)
-        : size(s)
-        , bytes((s + PAD * 2) * sizeof(T))
-    {
-        // Initialize guard with random data
-        if(PAD > 0)
-        {
-            rocblas_init_nan(guard, PAD);
-        }
-    }
-#else
     d_vector(size_t s)
         : size(s)
         , bytes(s ? s * sizeof(T) : sizeof(T))
     {
     }
-#endif
 
     T* device_vector_setup()
     {
@@ -57,75 +44,15 @@
             fmt::print(stderr, "Error allocating {} bytes ({} GB)\n", bytes, bytes >> 30);
             d = nullptr;
         }
-#ifdef ROCSOLVER_CLIENTS_TEST
-        else
-        {
-            if(PAD > 0)
-            {
-                // Copy guard to device memory before allocated memory
-                hipMemcpy(d, guard, sizeof(guard), hipMemcpyHostToDevice);
-
-                // Point to allocated block
-                d += PAD;
-
-                // Copy guard to device memory after allocated memory
-                hipMemcpy(d + size, guard, sizeof(guard), hipMemcpyHostToDevice);
-            }
-        }
-#endif
         return d;
     }
 
-    void device_vector_check(T* d)
-    {
-#ifdef ROCSOLVER_CLIENTS_TEST
-        if(PAD > 0)
-        {
-            U host[PAD];
-
-            // Copy device memory after allocated memory to host
-            hipMemcpy(host, d + this->size, sizeof(guard), hipMemcpyDeviceToHost);
-
-            // Make sure no corruption has occurred
-            EXPECT_EQ(memcmp(host, guard, sizeof(guard)), 0);
-
-            // Point to guard before allocated memory
-            d -= PAD;
-
-            // Copy device memory after allocated memory to host
-            hipMemcpy(host, d, sizeof(guard), hipMemcpyDeviceToHost);
-
-            // Make sure no corruption has occurred
-            EXPECT_EQ(memcmp(host, guard, sizeof(guard)), 0);
-        }
-#endif
-    }
+    void device_vector_check(T* d) {}
 
     void device_vector_teardown(T* d)
     {
         if(d != nullptr)
         {
-#ifdef ROCSOLVER_CLIENTS_TEST
-            if(PAD > 0)
-            {
-                U host[PAD];
-
-                // Copy device memory after allocated memory to host
-                hipMemcpy(host, d + this->size, sizeof(guard), hipMemcpyDeviceToHost);
-
-                // Make sure no corruption has occurred
-                EXPECT_EQ(memcmp(host, guard, sizeof(guard)), 0);
-
-                // Point to guard before allocated memory
-                d -= PAD;
-
-                // Copy device memory after allocated memory to host
-                hipMemcpy(host, d, sizeof(guard), hipMemcpyDeviceToHost);
-
-                // Make sure no corruption has occurred
-                EXPECT_EQ(memcmp(host, guard, sizeof(guard)), 0);
-            }
-#endif
             // Free device memory
             CHECK_HIP_ERROR((hipFree)(d));
         }
--- a/common/CMakeLists.txt
+++ b/common/CMakeLists.txt
@@ -1,5 +1,5 @@
 # ########################################################################
-# Copyright (c) 2021 Advanced Micro Devices, Inc.
+# Copyright (c) 2021-2022 Advanced Micro Devices, Inc.
 # ########################################################################
 
 add_library(rocsolver-common INTERFACE)
@@ -14,9 +14,6 @@
 prepend_path("${CMAKE_CURRENT_SOURCE_DIR}/src/" source_files source_paths)
 target_sources(rocsolver-common INTERFACE ${source_paths})
 target_compile_definitions(rocsolver-common INTERFACE __HIP_HCC_COMPAT_MODE__=1)
-target_compile_options(rocsolver-common INTERFACE
-  -Wno-unused-result # TODO: address [[nodiscard]] warnings
-)
 if(WIN32)
   target_compile_definitions(rocsolver-common INTERFACE
     WIN32_LEAN_AND_MEAN
--- a/common/include/common_host_helpers.hpp
+++ b/common/include/common_host_helpers.hpp
@@ -7,6 +7,7 @@
 #include <fstream>
 #include <iostream>
 #include <limits>
+#include <stdexcept>
 #include <string>
 #include <vector>
 
@@ -24,6 +25,16 @@
  * ===========================================================================
  */
 
+#define THROW_IF_HIP_ERROR(expr)                                                       \
+    do                                                                                 \
+    {                                                                                  \
+        hipError_t _status = (expr);                                                   \
+        if(_status != hipSuccess)                                                      \
+            throw std::runtime_error(fmt::format("{}:{}: [{}] {}", __FILE__, __LINE__, \
+                                                 hipGetErrorName(_status),             \
+                                                 hipGetErrorString(_status)));         \
+    } while(0)
+
 /* =============================================================================================== */
 /* Number properties functions.                                                                    */
 
@@ -162,7 +173,8 @@
                          const rocblas_fill uplo = rocblas_fill_full)
 {
     std::vector<T> hA(lda * n);
-    hipMemcpy(hA.data(), A + idx * stride, sizeof(T) * lda * n, hipMemcpyDeviceToHost);
+    THROW_IF_HIP_ERROR(
+        hipMemcpy(hA.data(), A + idx * stride, sizeof(T) * lda * n, hipMemcpyDeviceToHost));
 
     print_to_stream<T>(os, name, m, n, hA.data(), lda, uplo);
 }
@@ -181,8 +193,8 @@
 {
     std::vector<T> hA(lda * n);
     T* AA[1];
-    hipMemcpy(AA, A + idx, sizeof(T*), hipMemcpyDeviceToHost);
-    hipMemcpy(hA.data(), AA[0], sizeof(T) * lda * n, hipMemcpyDeviceToHost);
+    THROW_IF_HIP_ERROR(hipMemcpy(AA, A + idx, sizeof(T*), hipMemcpyDeviceToHost));
+    THROW_IF_HIP_ERROR(hipMemcpy(hA.data(), AA[0], sizeof(T) * lda * n, hipMemcpyDeviceToHost));
 
     print_to_stream<T>(os, name, m, n, hA.data(), lda, uplo);
 }
@@ -200,7 +212,8 @@
 {
     std::ofstream os(file);
     std::vector<T> hA(lda * n);
-    hipMemcpy(hA.data(), A + idx * stride, sizeof(T) * lda * n, hipMemcpyDeviceToHost);
+    THROW_IF_HIP_ERROR(
+        hipMemcpy(hA.data(), A + idx * stride, sizeof(T) * lda * n, hipMemcpyDeviceToHost));
 
     print_to_stream<T>(os, "", m, n, hA.data(), lda, uplo);
 }
@@ -219,8 +232,8 @@
     std::ofstream os(file);
     std::vector<T> hA(lda * n);
     T* AA[1];
-    hipMemcpy(AA, A + idx, sizeof(T*), hipMemcpyDeviceToHost);
-    hipMemcpy(hA.data(), AA[0], sizeof(T) * lda * n, hipMemcpyDeviceToHost);
+    THROW_IF_HIP_ERROR(hipMemcpy(AA, A + idx, sizeof(T*), hipMemcpyDeviceToHost));
+    THROW_IF_HIP_ERROR(hipMemcpy(hA.data(), AA[0], sizeof(T) * lda * n, hipMemcpyDeviceToHost));
 
     print_to_stream<T>(os, "", m, n, hA.data(), lda, uplo);
 }
--- a/common/src/common_host_helpers.cpp
+++ b/common/src/common_host_helpers.cpp
@@ -1,5 +1,5 @@
 /* ************************************************************************
- * Copyright (c) 2020-2021 Advanced Micro Devices, Inc.
+ * Copyright (c) 2020-2023 Advanced Micro Devices, Inc.
  * ************************************************************************ */
 
 #include <chrono>
@@ -23,7 +23,14 @@
  */
 double get_time_us()
 {
-    hipDeviceSynchronize();
+    hipError_t status = hipDeviceSynchronize();
+#ifdef ROCSOLVER_LIBRARY
+    if(status != hipSuccess)
+        fmt::print(std::cerr, "{}: [{}] {}\n", __PRETTY_FUNCTION__, hipGetErrorName(status),
+                   hipGetErrorString(status));
+#else
+    THROW_IF_HIP_ERROR(status);
+#endif
     return get_time_us_no_sync();
 }
 
@@ -31,6 +38,13 @@
  */
 double get_time_us_sync(hipStream_t stream)
 {
-    hipStreamSynchronize(stream);
+    hipError_t status = hipStreamSynchronize(stream);
+#ifdef ROCSOLVER_LIBRARY
+    if(status != hipSuccess)
+        fmt::print(std::cerr, "{}: [{}] {}\n", __PRETTY_FUNCTION__, hipGetErrorName(status),
+                   hipGetErrorString(status));
+#else
+    THROW_IF_HIP_ERROR(status);
+#endif
     return get_time_us_no_sync();
 }
--- a/library/src/lapack/roclapack_syevdx_heevdx_inplace.hpp
+++ b/library/src/lapack/roclapack_syevdx_heevdx_inplace.hpp
@@ -267,9 +267,14 @@
     // copy nev from device to host
     if(h_nev)
     {
-        hipMemcpyAsync(h_nev, d_nev, sizeof(rocblas_int) * batch_count, hipMemcpyDeviceToHost,
-                       stream);
-        hipStreamSynchronize(stream);
+        hipError_t status = hipMemcpyAsync(h_nev, d_nev, sizeof(rocblas_int) * batch_count,
+                                           hipMemcpyDeviceToHost, stream);
+        if(status != hipSuccess)
+            return get_rocblas_status_for_hip_status(status);
+
+        status = hipStreamSynchronize(stream);
+        if(status != hipSuccess)
+            return get_rocblas_status_for_hip_status(status);
     }
 
     return rocblas_status_success;
--- a/library/src/lapack/roclapack_syevj_heevj.hpp
+++ b/library/src/lapack/roclapack_syevj_heevj.hpp
@@ -1414,9 +1414,15 @@
         while(h_sweeps < max_sweeps)
         {
             // if all instances in the batch have finished, exit the loop
-            hipMemcpyAsync(&h_completed, completed, sizeof(rocblas_int), hipMemcpyDeviceToHost,
-                           stream);
-            hipStreamSynchronize(stream);
+            hipError_t status = hipMemcpyAsync(&h_completed, completed, sizeof(rocblas_int),
+                                               hipMemcpyDeviceToHost, stream);
+            if(status != hipSuccess)
+                return get_rocblas_status_for_hip_status(status);
+
+            status = hipStreamSynchronize(stream);
+            if(status != hipSuccess)
+                return get_rocblas_status_for_hip_status(status);
+
             if(h_completed == batch_count)
                 break;
 
--- a/library/src/lapack/roclapack_sygvdx_hegvdx_inplace.hpp
+++ b/library/src/lapack/roclapack_sygvdx_hegvdx_inplace.hpp
@@ -298,9 +298,14 @@
     // copy nev from device to host
     if(h_nev)
     {
-        hipMemcpyAsync(h_nev, d_nev, sizeof(rocblas_int) * batch_count, hipMemcpyDeviceToHost,
-                       stream);
-        hipStreamSynchronize(stream);
+        hipError_t status = hipMemcpyAsync(h_nev, d_nev, sizeof(rocblas_int) * batch_count,
+                                           hipMemcpyDeviceToHost, stream);
+        if(status != hipSuccess)
+            return get_rocblas_status_for_hip_status(status);
+
+        status = hipStreamSynchronize(stream);
+        if(status != hipSuccess)
+            return get_rocblas_status_for_hip_status(status);
     }
 
     rocblas_set_pointer_mode(handle, old_mode);
