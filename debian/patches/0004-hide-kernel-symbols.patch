From fbe30975c70a1cd92c27e842afd2bcf5185ec10a Mon Sep 17 00:00:00 2001
From: Cory Bloor <Cordell.Bloor@amd.com>
Date: Wed, 25 Jan 2023 06:44:17 -0700
Subject: [PATCH] Hide symbols for GPU kernels (#512)

The -fvisibility=hidden flag does not affect kernels and thus all
__global__ GPU functions become externally-visible symbols by default.
If a user defines a kernel with the same name and parameters as a
kernel that is defined in rocsolver, there will be a conflict.

There will not be any sort of compilation or linker error, but the
rocSOLVER definition of the function will be used instead of the user
definition. That is a problem, because our kernel function names do not
use any sort of prefix or namespace to prevent name conflicts with
functions defined by library users.

We have always built rocSOLVER in such a way that each translation unit
gets its own GPU code object in the final library binary. Unlike normal
template function instantiations, GPU template function instantiations
are not deduplicated by the linker. From the point of view of code
duplication, __global__ functions are already effectively static!

At some point, we should investigate whether rocSOLVER would benefit
from compiler options that combined the code objects to reduce the
amount of duplicated code in the library binary. For now, the default
behaviour means that there's no additional duplication introduced by
marking the GPU kernels as static --- not because there's no
duplication, but because everything that _would_ get duplicated already
has been duplicated.

In summary, marking __global__ kernels as static appears to be a simple
and effective way to prevent symbol conflicts between library and user
code.
---
 library/src/include/lib_macros.hpp | 6 +-----
 1 file changed, 1 insertion(+), 5 deletions(-)

diff --git a/library/src/include/lib_macros.hpp b/library/src/include/lib_macros.hpp
index 38a6fd88e..cc5743a2d 100644
--- a/library/src/include/lib_macros.hpp
+++ b/library/src/include/lib_macros.hpp
@@ -1,5 +1,5 @@
 /* ************************************************************************
- * Copyright (c) 2021 Advanced Micro Devices, Inc.
+ * Copyright (c) 2021-2023 Advanced Micro Devices, Inc.
  * ************************************************************************ */
 
 #pragma once
@@ -10,8 +10,4 @@
  * ===========================================================================
  */
 
-#ifdef _WIN32
 #define ROCSOLVER_KERNEL static __global__
-#else
-#define ROCSOLVER_KERNEL __global__
-#endif
